<?php

namespace App\Http\Controllers;

use App\Failure;
use App\Success;
use App\Imports\ProductsImport;
use App\Http\Requests\ProductsImportRequest;
use Maatwebsite\Excel\Facades\Excel;

class ProductsImportController extends Controller
{
    public function index()
    {
        $fails = Failure::all();
        $success = Success::all();
        return view('welcome' , compact('fails', 'success'));
    }

    public function store(ProductsImportRequest $request)
    {
        $file = $request->file('file')->store('import');
        $import = new ProductsImport;
        $import->import($file);
        
        return back()->with('message', 'Файл в черзі на завантаження...');
    }
}
