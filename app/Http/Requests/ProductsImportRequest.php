<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:xlsx,xls|max:'.substr(trim(ini_get('post_max_size')), 0, -1) * 1024
        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'Виберіть файл .xls, xlsx',
            'file.mimes' => 'Тільки .xls, xlsx файли',
            'file.max' => 'Максимальний розмір файлу - '. substr(trim(ini_get('post_max_size')), 0, -1) * 1024,
        ];
    }
}
