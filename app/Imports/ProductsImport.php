<?php

namespace App\Imports;


use App\Product;
use App\Category;
use App\Failure as FailsTable;
use App\Success;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;

class ProductsImport implements
        ToModel,
        WithStartRow,
        WithValidation,
        SkipsOnError,
        SkipsOnFailure,
        WithEvents,
        WithChunkReading,
        ShouldQueue
{
    use Importable, 
        SkipsErrors, 
        SkipsFailures, 
        RegistersEventListeners;

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        return new Product([
                'category_id' => $this->getCategoryId($row),
                'manufacturer' => $row[3],
                'name' => $row[4],
                'sku' => $row[5],
                'description' => $row[6],
                'price' => $row[7],
                'garantee' => $row[8],
                'isset' => $row[9] == 'есть в наличие' ? true : false,
            ]);

    }

    public function rules(): array
    {
        return [
            '*.0' => ['required'],
            '*.5' => ['unique:products,sku']
        ];
    }
    
    public function customValidationMessages()
    {
        return [
            '5.unique' => 'SKU уже є в базі',
            '0.required' => 'Структура рядка порушена, неможливо імпортувати.'
        ];
    }
    //separation file 
    public function chunkSize(): int
    {
        return 1000;
    }
    //writes errors to db
    public function onFailure(Failure ...$failures)
    {
        FailsTable::create([
            'row' => reset($failures)->row(),
            'error' => reset($failures)->errors()[0],
            'name' => reset($failures)->values()[4],
            'sku' => reset($failures)->values()[5],
        ]);
    }
    // cleans table with errors
    public static function beforeImport(BeforeImport $event)
    {
        FailsTable::truncate();
        Success::truncate();
    }
    
    //not worked, returnes 0
    // public function registerEvents(): array
    // {
    //     return [
    //         AfterImport::class => function(AfterImport $event) {
    //             Success::create([
    //                 'rows_imported' => $this->rows
    //             ]);
    //         },
    //     ];
    // }

    //create if not exists or get id of category
    public function getCategoryId($row)
    {
        if(!Category::where('category', $row[2])->first()
        && !Category::where('heading', $row[1])->first()
        && !Category::where('main_heading', $row[0])->first()){
            $mh = Category::create([
                'main_heading' => $row[0]
            ]);
            $head = Category::create([
                'heading' => $row[1],
                'parent_id' => $mh->id
            ]);
            $category = Category::create([
                'category' => $row[2],
                'parent_id' => $head->id
            ]);
            return $category->id;
        }
        elseif(!Category::where('category', $row[2])->first()
            && !Category::where('heading', $row[1])->first()
            && Category::where('main_heading', $row[0])->first()){
                $category = Category::create([
                    'heading' => $row[1],
                    'parent_id' => Category::where('main_heading', $row[0])->first()->id
                ]);
                return $category->id;
        }
        elseif(!Category::where('category', $row[2])->first()
            && Category::where('heading', $row[1])->first()){
                $category = Category::create([
                    'category' => $row[2],
                    'parent_id' => Category::where('heading', $row[1])->first()->id
                ]);
                return $category->id;
        }
        else{
            return Category::where('category', $row[2])->first()->id;
        }
    }
}
